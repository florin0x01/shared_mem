#include <iostream>
#include "clientserverexception.h"
#include "client.h"

int main() {
    Client cl("127.0.0.1", 19000);
    cl.connect_server();
    telegram  t = {
            "AAPL",
            "XYZ Inc",
        13,
        0
    };
    telegram  t2 = {
            "AAPL",
            "ABC Inc",
            30,
            0
    };
    telegram  t3 = {
            "AAPL",
            "Unknown Inc",
            20,
            0
    };
    telegram  t4 = {
            "ALA",
            "ABC Inc",
            130,
            0
    };
    telegram  t5 = {
            "IBM",
            "ABC Inc",
            20,
            0
    };

    cl.send_packet(t);
    cl.read_packet(t);
    std::cout << t.company << " " << t.symbol << " " << t.numOrdered << ", price: " << t.price << std::endl;
    cl.send_packet(t2);
    cl.read_packet(t2);
    std::cout << t2.company << " " << t2.symbol << " " << t2.numOrdered << ", price: " << t2.price << std::endl;
    cl.send_packet(t3);
    cl.read_packet(t3);
    std::cout << t3.company << " " << t3.symbol << " " << t3.numOrdered << ", price: " << t3.price << std::endl;
    cl.send_packet(t4);
    cl.read_packet(t4);
    std::cout << t4.company << " " << t4.symbol << " " << t4.numOrdered << ", price: " << t4.price << std::endl;
    cl.send_packet(t5);
    cl.read_packet(t5);
    std::cout << t5.company << " " << t5.symbol << " " << t5.numOrdered << ", price: " << t5.price << std::endl;
}
//
// Created by Florin on 11/11/2020.
//

#ifndef SHARED_MEM_SHAREDSPACE_H
#define SHARED_MEM_SHAREDSPACE_H

#include <iostream>
#include <string>
#include "telegram.h"

class SharedSpace {
private:
    char *shared_space_;
    int shm_handle_;
    const char* name_;
    int num_msg_;
    int capacity_;
    bool create_flag_;
public:
    SharedSpace(const char* name, int capacity=1024, bool create=true);
    ~SharedSpace();
    void send(const telegram&);
    void receive(telegram &, int microseconds=1000000);
    const char* getName() const;
    const int getCapacity() const;
    const int getNumberOfMessages() const;
};


#endif //SHARED_MEM_SHAREDSPACE_H

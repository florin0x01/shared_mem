#include <iostream>
#include "clientserverexception.h"
#include "sharedspace.h"
#include "server.h"
#include <fcntl.h>
#include <sys/mman.h>

int main() {
    SharedSpace incoming("blogic_mem_srv0x1", 1024, false);
    SharedSpace outgoing("blogic_mem_client0x1", 1024, false);

    Server srv;
    srv.start_connection();
    srv.accept_connection();

    telegram t;
    while (1) {
        bool packet_read = srv.read_packet(t);
        if (!packet_read) {
            if (errno == 60) { //timed out
                srv.close_connection();
                srv.accept_connection();
            }
            sleep(1);
            continue;
        }
        std::cout << "Message received" << std::endl;
        std::cout << t.company << " " << t.symbol << " " << t.numOrdered << std::endl;
        std::cout << "***" << std::endl;
        outgoing.send(t);
        incoming.receive(t);
        std::cout << "Num msg incoming: " << incoming.getNumberOfMessages() << std::endl;
        srv.send_packet(t);
    }
    return 0;
}

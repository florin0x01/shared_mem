//
// Created by Fl on 10/11/2020.
//

#include "telegram.h"
#include "server.h"
#include "clientserverexception.h"
#include <iostream>

Server::Server(int port) : port_(port) {
    sock_ = socket(PF_INET, SOCK_STREAM, 0);

    if (sock_ == -1) {
        throw ClientServerException("Could not create socket");
    }

    memset(&sock_addr, 0, sizeof(sock_addr));
    sock_addr.sin_family = AF_INET;
    sock_addr.sin_addr.s_addr = INADDR_ANY;
    sock_addr.sin_port = htons(port_);

    socklen_t t = 1;
    setsockopt(sock_, SOL_SOCKET, SO_REUSEADDR, &t, sizeof(int));

    //set second receive timeout
    struct timeval tv;
    tv.tv_sec = 3;
    tv.tv_usec = 0;
    setsockopt(sock_, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

    if (bind(sock_, reinterpret_cast<const sockaddr *>(&sock_addr), sizeof(sock_addr))) {
        throw ClientServerException("Could not bind socket");
    }
}

Server::~Server() {
    if (!listen_handle_) {
        close(sock_);
    }
}

void Server::start_connection() {
    listen_handle_ = listen(sock_, 0);
    if (listen_handle_) {
        throw ClientServerException("Could not listen");
    }
}

void Server::close_connection() {
    close(newsockfd_);
    newsockfd_ = -1;
}

void Server::accept_connection() {
    if (newsockfd_ == -2) {
        //still in accept connection
        return;
    }
    socklen_t clilen = sizeof(cli_addr);
    newsockfd_ = -2;
    std::cout << "Server::accept_connections" << std::endl;
    newsockfd_ = accept(sock_, (struct sockaddr *) &cli_addr, &clilen);
    if (newsockfd_ < 0) {
        throw ClientServerException("ERROR on accept");
    }
}

bool Server::read_packet(telegram& t) {
    if (newsockfd_ < 0) {
        return false;
    }
    bzero(&t, sizeof(t));
    int n = read(newsockfd_, &t, sizeof(t));

    if (n != sizeof(t)) {
       return false;
    }
    return true;
}


bool Server::send_packet(const telegram &t) {
    /* Send message to the server */
    int n = write(newsockfd_, &t, sizeof(t));
    if (n < 0) {
        return false;
    }
    return true;
}


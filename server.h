//
// Created by Fl on 10/11/2020.
//

#ifndef SHARED_MEM_SERVER_H
#define SHARED_MEM_SERVER_H


#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include "telegram.h"

class Server {
private:
    int port_;
    int sock_;
    int listen_handle_ = 1;
    int newsockfd_;
    struct sockaddr_in sock_addr, cli_addr;
public:
    Server(int port = 19000);

    ~Server();

    void start_connection();

    void close_connection();

    void accept_connection();

    bool read_packet(telegram &);

    bool send_packet(const telegram &);
};

#endif //SHARED_MEM_SERVER_H

#include "clientserverexception.h"

ClientServerException::ClientServerException(const char *what) : msg_(what) {

}

/** Constructor (C++ STL strings).
*  @param message The error message.
*/
ClientServerException::ClientServerException(const std::string &message) :
        msg_(message) {}

/** Destructor.
 * Virtual to allow for subclassing.
 */
ClientServerException::~ClientServerException() _NOEXCEPT {}

const char *ClientServerException::what() const _NOEXCEPT {
    return msg_.c_str();
}


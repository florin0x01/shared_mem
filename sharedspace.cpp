//
// Created by Florin on 11/11/2020.
//

#include "sharedspace.h"
#include <iostream>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/kernel.h>
#include "telegram.h"
#include "clientserverexception.h"
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/kernel.h>

//mlock ??

SharedSpace::SharedSpace(const char *name, int capacity, bool create) : name_(name), capacity_(capacity), num_msg_(0),
                                                                        create_flag_(create) {
    int shm_handle_, flags_;
    flags_ = O_CREAT | O_RDWR;
    if (!create) {
        flags_ = O_RDWR;
    }

    shm_handle_ = shm_open(name, flags_, S_IRWXU);

    if (shm_handle_ < 0) {
        throw ClientServerException(
                std::string(std::string("Shared space exception: ") + std::string(strerror(errno))).c_str());
    }

    if (create) {
        if (ftruncate(shm_handle_, sizeof(telegram) * capacity) == -1) {
            //if invalid arg, means already set
            //Invalid argument
            if (errno != 22) {
                throw ClientServerException(
                        std::string("Shared space exception capacity ") + std::string(strerror(errno)).c_str());
            }
        }
    }

    shared_space_ = (char *) mmap(0, sizeof(telegram) * capacity, PROT_READ | PROT_WRITE,
                                  MAP_SHARED, shm_handle_, 0);

    //  std::cout << "Shared space map for  " << name_ << ": " << std::hex << &shared_space_ << std::endl;
    printf("Shared map for %s: %p \n", name_, shared_space_);

    //read the number of messages currently
    memcpy(&num_msg_, shared_space_, sizeof(int));
    if (num_msg_ > capacity) { //bogus
        num_msg_ = 0;
    }
    std::cout << "Number of messages currently: " << num_msg_ << std::endl;

    std::cout << "Created shared space " << name << std::endl;
}

//mlock?
SharedSpace::~SharedSpace() {
    std::cout << "SharedSpace destructor for " << name_ << std::endl;
    close(shm_handle_);
    if (create_flag_) {
        if (shm_unlink(name_) == -1) {
            std::cerr << "Error removing shared space " << name_ << std::endl;
            std::cerr << strerror(errno) << std::endl;
        }
    }
    munmap(shared_space_, sizeof(telegram) * capacity_);
}

const char *SharedSpace::getName() const {
    return name_;
}

const int SharedSpace::getCapacity() const {
    return capacity_;
}

const int SharedSpace::getNumberOfMessages() const {
    return num_msg_;
}

void SharedSpace::receive(telegram &t, int microseconds) {
    int old_msg = num_msg_;
    while (old_msg == num_msg_) {
        memcpy(&old_msg, shared_space_, sizeof(int));
        usleep(microseconds);
    }
    num_msg_ = old_msg;
    if (num_msg_ > capacity_) {
        num_msg_ = 1;
    }
    memset(&t, 0, sizeof(t));
    memcpy(&t, shared_space_ + sizeof(int) + ((num_msg_ - 1) * sizeof(telegram)), sizeof(t));
}

void SharedSpace::send(const telegram &t) {
    num_msg_++;
    if (num_msg_ > capacity_) {
        num_msg_ = 1;
    }
    memcpy(shared_space_, &num_msg_, sizeof(int));
    memcpy(shared_space_ + sizeof(int) + ((num_msg_ - 1) * sizeof(telegram)), &t, sizeof(t));
}
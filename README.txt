Tested on MAC OS X. Should also work on Linux.
Build with Cmake.

Caveats: Of course, there is no fork/threading model for server. 
Also, there is no select() to receive incoming packets from kernel. This is just for demo purposes.
Server has timeout for existing connection. Only one connection / server (just for demo)
There can be some other potential bugs of course

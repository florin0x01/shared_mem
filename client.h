//
// Created by Fl on 10/11/2020.
//

#ifndef SHARED_MEM_CLIENT_H
#define SHARED_MEM_CLIENT_H

#include "telegram.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#endif //SHARED_MEM_CLIENT_H

class Client {
private:
    int sock_;
    struct in_addr ip_struct;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    const char *ip_;
    int port_;
public:
    Client(const char *ip, int port);
    ~Client();

    void connect_server();
    bool send_packet(const telegram &t);
    bool read_packet(telegram& t);
    void disconnect();
};
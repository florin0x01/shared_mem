//
// Created by Fl on 10/11/2020.
//

#include "client.h"
#include "clientserverexception.h"
#include <arpa/inet.h>

Client::Client(const char *ip, int port):ip_(ip), port_(port) {
    sock_ = socket(AF_INET, SOCK_STREAM, 0);
    if (!inet_aton(ip, &ip_struct)) {
        throw ClientServerException("Cannot parse IP addr");
    }

    if (sock_ < 0) {
        throw ClientServerException("ERROR opening socket");
    }
}

Client::~Client() {
    close(sock_);
}

void Client::connect_server() {
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy(&ip_struct, (char *)&serv_addr.sin_addr.s_addr, sizeof(ip_struct));
    serv_addr.sin_port = htons(port_);
    if (connect(sock_, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
        throw ClientServerException("ERROR connecting to server");
    }
}

bool Client::send_packet(const telegram &t) {
    /* Send message to the server */
    int n = write(sock_, &t, sizeof(t));
    if (n < 0) {
        return false;
    }
    return true;
}

bool Client::read_packet(telegram& resp) {
    /* Now read server response */
    bzero(&resp, sizeof(resp));
    int n = read(sock_, &resp, sizeof(resp));
    if (n != sizeof(resp)) {
       return false;
    }
    return true;
}

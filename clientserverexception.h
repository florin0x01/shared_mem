//
// Created by Fl on 10/11/2020.
//

#ifndef SHARED_MEM_CLIENTSERVEREXCEPTION_H
#define SHARED_MEM_CLIENTSERVEREXCEPTION_H

#include <exception>
#include <string>

class ClientServerException: public std::exception {
protected:
    /** Error message.
     */
    std::string msg_;
public:
    explicit ClientServerException(const char* what);
    /** Constructor (C++ STL strings).
    *  @param message The error message.
    */
    explicit ClientServerException(const std::string& message);

    /** Destructor.
     * Virtual to allow for subclassing.
     */
    virtual ~ClientServerException() _NOEXCEPT;

    virtual const char* what() const _NOEXCEPT;
};


#endif //SHARED_MEM_CLIENTSERVEREXCEPTION_H

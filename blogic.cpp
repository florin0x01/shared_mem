//
// Created by Florin on 10/11/2020.
//

#include <iostream>
#include <map>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/kernel.h>
#include "sharedspace.h"
#include "telegram.h"
#include <unistd.h>

using namespace std;

map<string, map<string, double>> stock_prices;

void populate_stock_prices() {
    stock_prices["AAPL"] = {
            {"regular", 100},
            {"ABC Inc", 90},
            {"XYZ Inc", 95}
    };
    stock_prices["IBM"] = {
            {"regular", 120},
            {"ABC Inc", 110},
            {"XYZ Inc", 112}
    };
}

void get_stock_price(telegram& t) {
    std::cout << "Received msg from company " << t.company << "for symbol " << t.symbol << std::endl;
    if (stock_prices.find(t.symbol) == stock_prices.end()) {
        memset(t.symbol, 0, sizeof(t.symbol));
        memcpy(t.symbol, "ERR_UNKNOWN", strlen("ERR_UNKNOWN"));
        return;
    }
    auto clients = stock_prices[t.symbol];
    if (clients.find(t.company) == clients.end()) {
        //regular price
        t.price = t.numOrdered * clients["regular"];
    } else {
        t.price = t.numOrdered * clients[t.company];
    }
}

int main() {
    populate_stock_prices();

    SharedSpace outgoing("blogic_mem_srv0x1", 1024, true);
    SharedSpace incoming("blogic_mem_client0x1", 1024, true);

    telegram t;
    while(1) {
        incoming.receive(t);
        get_stock_price(t);
        outgoing.send(t);
    }
}

